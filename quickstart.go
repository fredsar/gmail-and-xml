package main

import (
	"archive/zip"
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/gmail/v1"
	"google.golang.org/api/option"
)

func getClient(config *oauth2.Config) *http.Client {
	tokFile := "token.json"
	tok, err := tokenFromFile(tokFile)
	if err != nil {
		tok = getTokenFromWeb(config)
		saveToken(tokFile, tok)
	}
	return config.Client(context.Background(), tok)
}

func getTokenFromWeb(config *oauth2.Config) *oauth2.Token {
	authURL := config.AuthCodeURL("state-token", oauth2.AccessTypeOffline)
	fmt.Printf("Go to the following link in your browser then type the "+
		"authorization code: \n%v\n", authURL)

	var authCode string
	if _, err := fmt.Scan(&authCode); err != nil {
		log.Fatalf("Unable to read authorization code: %v", err)
	}

	tok, err := config.Exchange(context.TODO(), authCode)
	if err != nil {
		log.Fatalf("Unable to retrieve token from web: %v", err)
	}
	return tok
}

func tokenFromFile(file string) (*oauth2.Token, error) {
	f, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	tok := &oauth2.Token{}
	err = json.NewDecoder(f).Decode(tok)
	return tok, err
}

func saveToken(path string, token *oauth2.Token) {
	fmt.Printf("Saving credential file to: %s\n", path)
	f, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		log.Fatalf("Unable to cache oauth token: %v", err)
	}
	defer f.Close()
	json.NewEncoder(f).Encode(token)
}

func main() {
	srv, err := newGmailService()
	if err != nil {
		log.Fatalf("error setting up gmail service: %v", err)
	}
	parameters := "label:UNREAD after:2021-12-01 before:2022-01-01"
	if err := parseAllEmails(srv, parameters); err != nil {
		log.Fatalf("error parsing emails: %v", err)
	}
}

func parseAllEmails(srv *gmail.Service, parameters string) error {
	user := "me"
	m, err := srv.Users.Messages.List(user).Q(parameters).Do()
	if err != nil {
		return err
	}
	if len(m.Messages) == 0 {
		return fmt.Errorf("no messages found with given parameters: %s", parameters)
	}
	for _, l := range m.Messages {
		message, err := srv.Users.Messages.Get(user, l.Id).Do()
		if err != nil {
			return err
		}
		parts := message.Payload.Parts
		for _, part := range parts {
			err := saveValidAttatchment(srv, user, l.Id, part)
			if err != nil {
				return err
			}
		}
		_, err = srv.Users.Messages.Modify(user, l.Id, &gmail.ModifyMessageRequest{
			RemoveLabelIds: []string{"UNREAD"},
		}).Do()
		if err != nil {
			return err
		}
	}
	return nil
}

func newGmailService() (*gmail.Service, error) {
	ctx := context.Background()
	b, err := ioutil.ReadFile("credentials.json")
	if err != nil {
		return nil, err
	}

	// If modifying these scopes, delete your previously saved token.json.
	config, err := google.ConfigFromJSON(b, gmail.GmailModifyScope)
	if err != nil {
		return nil, err
	}
	client := getClient(config)

	srv, err := gmail.NewService(ctx, option.WithHTTPClient(client))
	if err != nil {
		return nil, err
	}
	return srv, nil
}

func saveValidAttatchment(srv *gmail.Service, user, messageID string, part *gmail.MessagePart) error {
	if part.MimeType == "multipart/mixed" {
		for _, innerPart := range part.Parts {
			attach, err := srv.Users.Messages.Attachments.Get(user, messageID, innerPart.Body.AttachmentId).Do()
			if err != nil {
				return err
			}
			decoded, err := base64.URLEncoding.DecodeString(attach.Data)
			if err != nil {
				return err
			}
			if !strings.Contains(string(decoded[:10]), "xml") {
				continue
			}
			if err := createFile(fmt.Sprintf("%s.xml", messageID), attach.Data); err != nil {
				return err
			}
		}
	} else if part.MimeType == "application/zip" {
		attach, err := srv.Users.Messages.Attachments.Get(user, messageID, part.Body.AttachmentId).Do()
		if err != nil {
			return err
		}
		if err := createFile(fmt.Sprintf("%s.zip", messageID), attach.Data); err != nil {
			return err
		}
		if err := parseZipFile(messageID); err != nil {
			return err
		}
	}
	return nil
}

func createFile(filename, toWrite string) error {
	f, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer f.Close()

	dec, err := base64.URLEncoding.DecodeString(toWrite)
	if err != nil {
		return err
	}
	if _, err := f.Write(dec); err != nil {
		return err
	}
	if err := f.Sync(); err != nil {
		return err
	}
	return nil
}

func unzipFile(src string, dest string) error {
	r, err := zip.OpenReader(src)
	if err != nil {
		return err
	}
	defer r.Close()

	for _, f := range r.File {

		// Store filename/path for returning and using later on
		fpath := filepath.Join(dest, f.Name)

		// Check for ZipSlip. More Info: http://bit.ly/2MsjAWE
		if !strings.HasPrefix(fpath, filepath.Clean(dest)+string(os.PathSeparator)) {
			return fmt.Errorf("%s: illegal file path", fpath)
		}

		if f.FileInfo().IsDir() {
			// Make Folder
			os.MkdirAll(fpath, os.ModePerm)
			continue
		}

		// Make File
		if err = os.MkdirAll(filepath.Dir(fpath), os.ModePerm); err != nil {
			return err
		}

		outFile, err := os.OpenFile(fpath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, f.Mode())
		if err != nil {
			return err
		}

		rc, err := f.Open()
		if err != nil {
			return err
		}

		_, err = io.Copy(outFile, rc)

		// Close the file without defer to close before next iteration of loop
		outFile.Close()
		rc.Close()

		if err != nil {
			return err
		}
	}
	return nil
}

func parseZipFile(messageID string) error {
	err := unzipFile(fmt.Sprintf("%s.zip", messageID), messageID)
	if err != nil {
		return err
	}

	innerFiles, err := getInnerXML(fmt.Sprintf("./%s", messageID))
	if err != nil {
		return err
	}
	for _, f := range innerFiles {
		dest := strings.Split(f, "/")[2]
		bytesRead, err := ioutil.ReadFile(f)
		if err != nil {
			return err
		}
		err = ioutil.WriteFile(dest, bytesRead, 0644)
		if err != nil {
			return err
		}
	}
	if err := removeZipAndItsDirectory(messageID); err != nil {
		return err
	}
	return nil
}

func getInnerXML(dir string) ([]string, error) {
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		return nil, err
	}
	toReturn := make([]string, 0)
	for _, f := range files {
		if f.IsDir() {
			data, err := getInnerXML(fmt.Sprintf("%s/%s", dir, f.Name()))
			if err != nil {
				return nil, err
			}
			for _, d := range data {
				if strings.Contains(d, ".XML") || strings.Contains(d, ".xml") {
					toReturn = append(toReturn, d)
				}
			}
		} else if strings.Contains(f.Name(), ".XML") || strings.Contains(f.Name(), ".xml") {
			dirToAppend := dir[2:] + "/"
			toReturn = append(toReturn, fmt.Sprintf("%s%s", dirToAppend, f.Name()))
		}
	}
	return toReturn, nil
}

func removeZipAndItsDirectory(messageID string) error {
	if err := os.RemoveAll(messageID); err != nil {
		return err
	}

	if err := os.Remove(fmt.Sprintf("%s.zip", messageID)); err != nil {
		return err
	}

	return nil
}
